# lbenv-deployment
This project host tools and configuration files needed to help deploying LHCb
utilities and environment scripts (see [LbEnv](https://gitlab.cern.ch/lhcb-core/LbEnv)).

This is split into two components:

* **LbEnv environments:** These are created with conda from the `data/*-environment.yml` files using `lbtaskrun`. See [here](https://lbdevops.web.cern.ch/lbdevops/SoftwareReleaseGuide.html#deployment-of-lbenv) for more details.
* **Contrib RPM:** Used control the default versions of the external utilities such as ganga.

## Contrib RPM

The version of the contrib packages to install and the creation of the symlinks
required to make them available in the LHCb environment is delegated to an RPM
package which _spec_ file is hosted in the [`rpm`](rpm) directory.

The RPM is built via a Gitlab-CI job (see [.gitlab-ci.yml](.gitlab-ci.yml)).

At every new commit pushed to the Gitlab server, a Gitlab-CI job compares
version and release numbers in [`rpm/ContribSelection.spec`](rpm/ContribSelection.spec)
with the RPMs in [https://lhcb-rpm.web.cern.ch/lhcb-rpm/incubator](https://lhcb-rpm.web.cern.ch/lhcb-rpm/incubator), and, if not present, builds the new RPM and deploys it to EOS.
