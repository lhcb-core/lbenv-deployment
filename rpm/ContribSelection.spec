Name:       ContribSelection
Version:    3
Release:    8
Vendor:     LHCb
Summary:    Symlinks to default versions of contrib packages for LHCb.
License:    GPLv3

Group: LHCb

BuildArch: noarch
AutoReqProv: no
Prefix: /opt/LHCbSoft
Provides: /bin/sh
Provides: /bin/bash
Requires: CMake_3.16.2
# For SLC5
Requires: CMake_3.3.2
Requires: ninja_1.8.2.g81279.kitware.dyndep-1.jobserver-1
Requires: CMT = v1r20p20090520
Requires: glimpse_4.18.6
Requires: CondDBBrowser = 0.1.1

%description
Symlinks to default versions of contrib packages for LHCb.

%prep
# we have no source, so nothing here

%build
rm -rf Linux-x86_64 x86_64-slc5
mkdir Linux-x86_64 x86_64-slc5

# --- CMT ---
cat > Linux-x86_64/cmt <<EOF
#!/bin/bash --norc
export MYSITEROOT=<<prefix>>
export SITEROOT=\${MYSITEROOT}
export CMTSITE=LOCAL
export CMTROOT=\${MYSITEROOT}/contrib/CMT/v1r20p20090520
export CMTBIN=Linux-x86_64
export CMTPROJECTPATH=\${CMTPROJECTPATH:+\$CMTPROJECTPATH:}\${CMAKE_PREFIX_PATH}
export CMTCONFIG=\${CMTCONFIG:-\$BINARY_TAG}
# unset bash functions
unset -f \$(printenv | sed -n 's/^BASH_FUNC_\([^=]*\)()=.*/\1/p')
exec \${CMTROOT}/\${CMTBIN}/cmt "\$@"
EOF
chmod a+x Linux-x86_64/cmt

mkdir x86_64-centos7
# mkdir x86_64-slc6

# --- CondDBBrowser ---
ln -s ../../contrib/CondDBBrowser/0.1.1/bin/CondDBBrowser x86_64-centos7

# --- lb-docker-run ---
curl -LO  https://gitlab.cern.ch/lhcb-core/LbDocker/raw/c3a0e79d0c41c477ed4c75c5292005b4398f4e15/scripts/lb-docker-run
chmod a+x lb-docker-run

# --- Ganga wrapper ---
cat > ganga <<EOF
#!/bin/sh
GUI=false
# if the first command line option is --ganga-version use the version of
# Ganga specified as argument
GANGA_VERSION=LATEST
while [ \$# -ne 0 ] ; do
    case \$1 in
        --ganga-version)
          shift
          GANGA_VERSION=\$1
          shift
          ;;
        --gui)
          GUI=true
          shift
          ;;
        *) break ;;
    esac
done

export GANGA_CONFIG_PATH=\${GANGA_CONFIG_PATH:-GangaLHCb/LHCb.ini}
export GANGA_SITE_CONFIG_AREA=\${GANGA_SITE_CONFIG_AREA:-/cvmfs/lhcb.cern.ch/lib/GangaConfig/config}
# Pretty pointless, but Ganga (<=7.1.9) requires it, even if empty
export ROOTSYS=
if [ "\$GUI" = true ]
then
    exec /cvmfs/ganga.cern.ch/Ganga/install/\${GANGA_VERSION}/bin/ganga-gui "\$@"
else
    exec /cvmfs/ganga.cern.ch/Ganga/install/\${GANGA_VERSION}/bin/ganga "\$@"
fi
EOF
chmod a+x ganga


%install
mkdir -p %{buildroot}/opt/LHCbSoft/bin
cp -av . %{buildroot}/opt/LHCbSoft/bin/

%post -p /bin/bash
sed -i "s#<<prefix>>#${RPM_INSTALL_PREFIX}#g" ${RPM_INSTALL_PREFIX}/bin/Linux-x86_64/cmt

%files
%defattr(-,root,root)
/opt/LHCbSoft

%changelog
# let skip this for now
